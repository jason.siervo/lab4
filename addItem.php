<?php include('includes/header.php'); ?>

<?php include('includes/navbar_login.php'); ?>

<?php
    session_start();

    if(!isset($_SESSION['username1']))
    {
        header('Location:login_admin.php');
    }

    include('includes/dbcon.php');

    if(isset($_POST['add'])){
        
        $blood_type = $_POST['blood_type'];
        $donor = $_POST['donor'];
        $hospital = $_POST['hospital'];

        $err = 0;

        if(empty($blood_type)){
            $err =  1;
            $blood_typeErr = "Blood type is required";
        }
        if(!preg_match("([a-bA-b-+]{2,3})", $blood_type)){
            $err =  1;
            $blood_typeErr = "Please enter a valid blood type.";
        }

        if(empty($donor)){
            $err =  1;
            $donorErr = "Name of donor is required";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $donor)){
            $err =  1;
            $donorErr = "Name must contain letters only";
        }

        if(empty($hospital)){
            $err =  1;
            $hospitalErr = "Name of hospital is required";
        }
        if(!preg_match("([a-zA-Z0-9]{5})", $hospital)){
            $err =  1;
            $hospitalErr = "Hospital must have at least 5 characters";
        }

        if($err == 0) {
            session_start();
            $res = $mysqli->query("INSERT INTO inventory(blood_type,donor,hospital) VALUES('$blood_type', '$donor', '$hospital')");
            header("Location: inventory.php");
        }
    }


?>


<div class="part1_copy">
    <div class="col-md-4"> </div>
    <div class="col-md-4"> 
    
    <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">

        <br>
        <h2>ADD ITEM</h2>


        <div class="form-group">
            <label>Blood Type</label>
            <input type="text" name="blood_type" value="<?php if(isset($_POST['blood_type'])){echo $blood_type;} ?>" class="form-control" placeholder="Blood Type">
            <?php
                if(isset($blood_typeErr)){
                    echo "<div class='alert alert-danger'>$blood_typeErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Donor</label>
            <input type = "text" name="donor" value="<?php if(isset($_POST['donor'])){echo $donor;} ?>" class="form-control" placeholder="Donor"></textarea>
            <?php
                if(isset($donorErr)){
                    echo "<div class='alert alert-danger'>$donorErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Hospital</label>
            <input type="text" name="hospital" value="<?php if(isset($_POST['hospital'])){echo $hospital;} ?>" class="form-control" placeholder="hospital">
            <?php
                if(isset($hospitalErr)){
                    echo "<div class='alert alert-danger'>$hospitalErr</div>";
                }
            ?>
        </div>

        <button type="submit" name="add" class="btn btn-info">Submit</button>

    </form>
    
    </div>
    <div class="col-md-4"> </div>
</div>

<?php include('includes/footer.php'); ?>
