<?php include('includes/header.php') ?>

<?php include('includes/navbar_admin.php') ?>

<?php 

    session_start();

    if(!isset($_SESSION['username1']))
    {
        header('Location:login_admin.php');
    }

    include('includes/dbcon.php');

    $res = $mysqli->query("SELECT * FROM tblusers");
?>


<div class = "part1_copy">

    <section>
        <div class="tbl-header">
            <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>E-mail</th>
                    <th>address2</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Actions</th>
                </tr>
            </thead>
            </table>
        </div>   

        <div class="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <?php  while($row=$res->fetch_array()) { ?> 
                <tr>
                    <td> <?= $row['id']?> </td>
                    <td> <?= $row['fname']?> </td>
                    <td> <?= $row['lname']?> </td>
                    <td> <?= $row['username']?> </td>
                    <td> <?= $row['pass']?> </td>
                    <td> <?= $row['email']?> </td>
                    <td> <?= $row['address2']?> </td>
                    <td> <?= $row['created_at']?> </td>
                    <td> <?= $row['updated_at']?> </td>
                    <td>
                        <a href="updateUser.php?id=<?= $row['id'] ?>" class="btn btn-warning"><span class="fa fa-edit"></span></a>
                        <a href="deleteUser.php?id=<?= $row['id'] ?>" onclick="return confirm('Do you really want to delete?')" class="btn btn-danger"><span class="fa fa-trash"></span></a>
                    </td>

                </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
        <br style="clear:both;"/>
    </section>

    <center>
    <a href="addUser.php" class = "btn btn-success">Add User</a>
    <center>
</div>

<?php include('includes/footer.php') ?>