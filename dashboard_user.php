<?php include('includes/header.php') ?>

<?php include('includes/navbar_user.php') ?>

<?php

    include('includes/dbcon.php');

    session_start();    

    if(!isset($_SESSION['username1']))
    {
        header('Location:login_user.php');
    }

    $username1 = $_SESSION['username1'];
    $pass = $_SESSION['pass'];
    
    $res = $mysqli->query("SELECT * FROM tblusers WHERE username = '" . $_SESSION['username1']."' LIMIT 1");
    
?>

<div class = "part1_copy">

    <div class="row">
        <div class="col-md-1"> </div>
        <div class="col-md-10"> 
        <h1>
            User Information
        </h1>

        <table class ="table">
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
                <th>Password</th>
                <th>E-mail</th>
                <th>Address</th>
                <th>Address</th>
                <th>Time Created</th>
                <th>Time Updated</th>
            </tr>
            <?php
                while($row=$res->fetch_array()) {
            ?>
            <tr>
                <td> <?= $row['id']?> </td>
                <td> <?= $row['fname']?> </td>
                <td> <?= $row['lname']?> </td>
                <td> <?= $row['username']?> </td>
                <td> <?= $row['pass']?> </td>
                <td> <?= $row['email']?> </td>
                <td> <?= $row['address1']?> </td>
                <td> <?= $row['address2']?> </td>
                <td> <?= $row['created_at']?> </td>
                <td> <?= $row['updated_at']?> </td>
                <td></td>
            </tr>
            <?php } ?>
            
        </table>
        
        </div>
        <div class="col-md-1"> </div>
    </div>

</div>

<?php include('includes/footer.php') ?>