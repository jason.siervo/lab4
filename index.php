<?php include('includes/header.php') ?>

<?php include('includes/navbar.php') ?>

<?php
	include('includes/dbcon.php');

	if(isset($_POST['submit'])){
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];		
        $username1 = $_POST['username1'];
        $email = $_POST['email']; 
        $pass = $_POST['pass'];
		$cpass = $_POST['cpass'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];

        $err = 0;

        if(empty($fname)){
            $err =  1;
            $fnameErr = "First name is required";
		}
		if(!preg_match("([a-zA-Z]{3})", $fname)){
            $err =  1;
            $fnameErr = "First name must have at least 3 characters";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $fname)){
            $err =  1;
            $fnameErr = "First name must contain letters only";
		}
		
		if(empty($lname)){
            $err =  1;
            $lnameErr = "Last name is required";
		}
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $fname)){
            $err =  1;
            $lnameErr = "Last name must contain letters only";
        }

		if(empty($username1)){
            $err =  1;
            $usernameErr = "Username is required";
		}
        if(!preg_match("([a-zA-Z0-9]{5})", $username1)){
            $err =  1;
            $usernameErr = "Username must have at least 5 characters";
		}
		
        if(empty($pass)){
            $err =  1;
            $passErr = "Password is required";
        } 
        if(!preg_match("([a-zA-Z0-9]{8})", $pass)){
            $err =  1;
            $passErr = "Password must have at least 8 characters";
        }
        if(empty($cpass)){
            $err =  1;
            $cpassErr = "Password confirmation is required";
        } 
        if(!strcmp('$pass','$cpass')!=0){
            $err =  1;
            $cpassErr = "Password field does not match with Confirm password field";
		}
		
		if(empty($email)){
            $err =  1;
            $emailErr = "Email field is required";
        }
        if(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+\.([a-zA-Z0-9\._-]+)+$/", $email)){
            $err =  1;
            $emailErr = "Email must be in correct format";
		}

        //Additional Validations
        //Activity

        if($err == 0){
            session_start();
			$_SESSION['fname'] = $fname;
			$_SESSION['lname'] = $lname;
            $_SESSION['username1'] = $username1;
            $_SESSION['email'] = $email;
			$_SESSION['pass'] = $pass;
			$_SESSION['address1'] = $address1;
			$_SESSION['address2'] = $address2;

			$SQL = $mysqli->query("INSERT INTO tblusers(fname, lname, username, pass, email, address1, address2) VALUES('$fname', '$lname',  '$username1', '$pass', '$email', '$address1', '$address2')");
			
            header("Location: dashboard_user.php");
        }

    }
?>

<div class = "part1">
	<center>

			<div class = "par1_1">
				<p class = "font1">Learn how <b>YOU</b> could<br> help in other ways.</p>
			</div>
			
			<div class = "par1_1">
				<p class = "font2">Can't donate blood? <b>You</b> can<br> still help by being a volunteer for PRC.
				<br>Just register down below.<br>
				</p>
			</div>
			
			<div class = "button_position">
				<a href="http://www.redcross.org.ph/what-we-do/social-services" class="btn btn-outline btn-xl page-scroll">Know more about us</a>
			</div>
			
			<a href = "#part2"> <img src="images/new/down_button.png" class="bounce"> </a>
		
	</center>
</div>

<div id = "part2">
	<center>
		<p class = "font3">Let's get started.</p>
	</center>	
</div>

<div id = "part3">
	<div class="col-md-3"></div>

	<form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
		<div class="col-md-3"> 

			<div class="form-group">
				<input type="text" name="fname" value="<?php if(isset($fname)){echo $fname;} ?>" class="form-control" placeholder="First Name">
				<?php
					if(isset($fnameErr)){
						echo "<div class='alert alert-danger'>$fnameErr</div>";
					}
				?>
			</div>
			
			<div class="form-group">
				<input type="text" name="username1" value="<?php if(isset($username1)){echo $username1;} ?>" class="form-control" placeholder="Username">
				<?php
					if(isset($usernameErr)){
						echo "<div class='alert alert-danger'>$usernameErr</div>";
					}
				?>
			</div>

			<div class="form-group">
				<input type="text" name="address1" value="<?php if(isset($address1)){echo $address1;} ?>" class="form-control" placeholder="House number, Street, Municipality">
				<?php
					if(isset($address1Err)){
						echo "<div class='alert alert-danger'>$address1Err</div>";
					}
				?>
			</div>
			
			<div class="form-group">
				<input type="password" name="pass" value="<?php if(isset($pass)){echo $pass;} ?>" class="form-control" placeholder="Password">
				<?php
					if(isset($passErr)){
						echo "<div class='alert alert-danger'>$passErr</div>";
					}
				?>
			</div>

		</div>

		<div class="col-md-3">

		<div class="form-group">
				<input type="text" name="lname" value="<?php if(isset($lname)){echo $lname;} ?>" class="form-control" placeholder="Last Name">
				<?php
					if(isset($lnameErr)){
						echo "<div class='alert alert-danger'>$lnameErr</div>";
					}
				?>
			</div>

			<div class="form-group">
				<input type="email" name="email" value="<?php if(isset($email)){echo $email;} ?>" class="form-control" placeholder="E-mail">
				<?php
					if(isset($emailErr)){
						echo "<div class='alert alert-danger'>$emailErr</div>";
					}
				?>
			</div>

			<div class="form-group">
				<input type="text" name="address2" value="<?php if(isset($address2)){echo $address2;} ?>" class="form-control" placeholder="City, Country, Zip Code">
				<?php
					if(isset($address2Err)){
						echo "<div class='alert alert-danger'>$address2Err</div>";
					}
				?>
			</div>
			
			<div class="form-group">
				<input type="password" name="cpass" value="<?php if(isset($pass)){echo $pass;} ?>" class="form-control" placeholder="Confirm Password">
				<?php
					if(isset($cpassErr)){
						echo "<div class='alert alert-danger'>$cpassErr</div>";
					}
				?>
			</div>
		</div>
	
	<div class="col-md-3"></div>
	
</div>

<br style="clear:both;"/>

<div id = "part4">
	<center>
		<button type="reset" value = "Reset all the fields" name = "reset" class="btn btn-default">Delete</button>
		<button type="submit" name = "submit"  class="btn btn-primary">Submit</button>
	</center>	
	</form>
</div>

<?php include('includes/modal_user.php') ?>

<?php include('includes/footer.php') ?>
