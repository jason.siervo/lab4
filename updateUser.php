<?php include('includes/header.php'); ?>

<?php include('includes/navbar_login.php'); ?>

<?php

    session_start();    

    if(!isset($_SESSION['username1']))
    {
        header('Location:login_admin.php');
    }

    include('includes/dbcon.php');

    if(isset($_GET['id'])){
        $id = $_GET['id'];

        $res = $mysqli->query("SELECT * FROM tblusers WHERE id = $id");
        $row = $res->fetch_array();
    }
    else{
        $id = $_POST['id'];

        $res = $mysqli->query("SELECT * FROM tblusers WHERE id = $id");
        $row = $res->fetch_array();
    }

    if(isset($_POST['update'])){
        $id = $_POST['id'];
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $username1 = $_POST['username1'];
        $pass = $_POST['pass'];
        $email = $_POST['email'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];

        $err = 0;
        
        if(empty($fname)){
            $err =  1;
            $fnameErr = "First name is required";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $fname)){
            $err =  1;
            $fnameErr = "First name must contain letters only";
        }

        if(empty($lname)){
            $err =  1;
            $nameErr = "Last name is required";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $lname)){
            $err =  1;
            $lnameErr = "Last name must contain letters only";
        }

        if(empty($username1)){
            $err =  1;
            $username1Err = "Username is required";
        }
        if(!preg_match("([a-zA-Z0-9]{5})", $username1)){
            $err =  1;
            $username1Err = "Username must have at least 5 characters";
        }
        
        if($err == 0) {

            $SQL = $mysqli->query("UPDATE tblusers SET fname = '$fname', lname = '$lname', username = '$username1', pass = '$pass', email = '$email', address1 = '$address1', address2 = '$address2' WHERE id = $id");
            header("Location: members.php");
        }
    }
?>


<div class="part1_copy2">
    <div class="col-md-4"> </div>
    <div class="col-md-4"> 
    
        <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
            <br>
            <h2>UPDATE USER INFORMATION</h2>

            <input type="hidden" name="id" value="<?= $row['id'] ?>">
            <div class="form-group">
                <label>First Name</label>
                <input type="text" name="fname" value="<?= $row['fname'] ?>" class="form-control" placeholder="fname">
            </div>

            <div class="form-group">
                <label>Last Name</label>
                <input type="text" name="lname" value="<?= $row['lname'] ?>" class="form-control" placeholder="lname">
            </div>

            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username1" value="<?= $row['username'] ?>" class="form-control" placeholder="Username">
            </div>

            <div class="form-group">
                <label>Password</label>
                <input type="text" name="pass" value="<?= $row['pass'] ?>" class="form-control" placeholder="Password">
            </div>

            <div class="form-group">
                <label>E-mail</label>
                <input type="text" name="email" value="<?= $row['email'] ?>" class="form-control" placeholder="E-mail">
            </div>

            <div class="form-group">
                <label>Address</label>
                <input type="text" name="address1" value="<?= $row['address1'] ?>" class="form-control" placeholder="Address">
            </div>

            <div class="form-group">
                <label>Address</label>
                <input type="text" name="address2" value="<?= $row['address2'] ?>" class="form-control" placeholder="Address">
            </div>

            <button type="submit" name="update" class="btn btn-info">Submit</button>

        </form>
    
    </div>
    <div class="col-md-4"> </div>
</div>

<?php include('includes/footer.php'); ?>
