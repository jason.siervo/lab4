<?php include('includes/header.php'); ?>

<?php include('includes/navbar_login.php'); ?>

<?php

session_start();

    if(!isset($_SESSION['username1']))
    {
        header('Location:login_admin.php');
    }

    include('includes/dbcon.php');

    if(isset($_GET['id'])){
        $id = $_GET['id'];

        $res = $mysqli->query("SELECT * FROM inventory WHERE id = $id");
        $row = $res->fetch_array();
    }
    else{
        $id = $_POST['id'];

        $res = $mysqli->query("SELECT * FROM inventory WHERE id = $id");
        $row = $res->fetch_array();
    }

    if(isset($_POST['update'])){
        $id = $_POST['id'];
        $blood_type = $_POST['blood_type'];
        $donor = $_POST['donor'];
        $hospital = $_POST['hospital'];

        $err = 0;

        if(empty($blood_type)){
            $err =  1;
            $blood_typeErr = "Blood type is required";
        }
        if(!preg_match("([a-bA-b-+]{2,3})", $blood_type)){
            $err =  1;
            $blood_typeErr = "Please enter a valid blood type.";
        }

        if(empty($donor)){
            $err =  1;
            $donorErr = "Name of donor is required";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $donor)){
            $err =  1;
            $donorErr = "Name must contain letters only";
        }

        if(empty($hospital)){
            $err =  1;
            $hospitalErr = "Name of hospital is required";
        }
        if(!preg_match("([a-zA-Z0-9]{5})", $hospital)){
            $err =  1;
            $hospitalErr = "Hospital must have at least 5 characters";
        }

        if($err == 0) {
            $SQL = $mysqli->query("UPDATE inventory SET blood_type = '$blood_type', donor = '$donor', hospital = '$hospital' WHERE id = $id");
            header("Location: inventory.php");
        }
    }
?>


<div class="part1_copy">
    <div class="col-md-4"> </div>
    <div class="col-md-4"> 
    
        <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
            <br>
            <h2> UPDATE ITEM FORM  </h2>

            <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
            <div class="form-group">
                <label>Blood Type</label>
                <input type="text" name="blood_type" value="<?= $row['blood_type'] ?>" class="form-control" placeholder="Blood Type">
            </div>

            <div class="form-group">
                <label>Donor</label>
                <input type="text" name="donor" value="<?= $row['donor'] ?>" class="form-control" placeholder="Donor">
            </div>

            <div class="form-group">
                <label>Hospital</label>
                <input type="text" name="hospital" value="<?= $row['hospital'] ?>" class="form-control" placeholder="hospital">
            </div>

            <button type="submit" name="update" class="btn btn-info">Submit</button>

        </form>
    
    </div>
    <div class="col-md-4"> </div>
</div>

<?php include('includes/footer.php'); ?>
