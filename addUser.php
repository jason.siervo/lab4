<?php include('includes/header.php'); ?>

<?php include('includes/navbar_login.php'); ?>

<?php
    include('includes/dbcon.php');

    session_start();
    
    if(!isset($_SESSION['username1']))
    {
        header('Location:login_admin.php');
    }

    if(isset($_POST['add'])){
        
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $username1 = $_POST['username1'];
        $pass = $_POST['pass'];
        $cpass = $_POST['cpass'];
        $email = $_POST['email'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];

        $err = 0;

        if(empty($fname)){
            $err =  1;
            $fnameErr = "First name is required";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $fname)){
            $err =  1;
            $fnameErr = "First name must contain letters only";
        }

        if(empty($lname)){
            $err =  1;
            $nameErr = "Last name is required";
        }
        if(!preg_match("/^[_a-zA-Z\\s]+$/", $lname)){
            $err =  1;
            $lnameErr = "Last name must contain letters only";
        }

        if(empty($username1)){
            $err =  1;
            $username1Err = "Username is required";
        }
        if(!preg_match("([a-zA-Z0-9]{5})", $username1)){
            $err =  1;
            $username1Err = "Username must have at least 5 characters";
        }

        if(empty($pass)){
            $err =  1;
            $passErr = "Password is required";
        }
        if(!preg_match("([a-zA-Z0-9]{8})", $pass)){
            $err =  1;
            $passErr = "Password must have at least 8 characters";
        }

        if(empty($cpass)){
            $err =  1;
            $cpassErr = "Password confirmation is required";
        }
        if(!strcmp('$pass','$cpass')!=0){
            $err =  1;
            $cpassErr = "Password field does not match with Confirm password field";
        }

        if(empty($email)){
            $err =  1;
            $emailErr = "E-mail is required";
        }
        if(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+\.([a-zA-Z0-9\._-]+)+$/", $email)){
            $err =  1;
            $emailErr = "Email must be in correct format";
        }

        if($err == 0) {
            session_start();
            $res = $mysqli->query("INSERT INTO tblusers(fname,lname,username, pass, email, address1, address2) VALUES('$fname', '$lname', '$username1', '$pass', '$email', '$address1', '$address2')");
            header("Location: members.php");
        }
    
    }


?>


<div class="part1_copy2">
    <div class="col-md-4"> </div>
    <div class="col-md-4"> 
    <br>
    <h2>ADD USER</h2>
    <form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
        <div class="form-group">
            <label>First Name</label>
            <input type="text" name="fname" value="<?php if(isset($_POST['fname'])){echo $fname;} ?>" class="form-control" placeholder="First Name">
            <?php
                if(isset($fnameErr)){
                    echo "<div class='alert alert-danger'>$fnameErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Last Name</label>
            <input type = "text" name="lname" value="<?php if(isset($_POST['lname'])){echo $lname;} ?>" class="form-control" placeholder="Last Name">
            <?php
                if(isset($lnameErr)){
                    echo "<div class='alert alert-danger'>$lnameErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username1" value="<?php if(isset($_POST['username1'])){echo $username1;} ?>" class="form-control" placeholder="Username">
            <?php
                if(isset($username1Err)){
                    echo "<div class='alert alert-danger'>$username1Err</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Password</label>
            <input type="password" name="pass" value="<?php if(isset($_POST['pass'])){echo $pass;} ?>" class="form-control" placeholder="Password">
            <?php
                if(isset($passErr)){
                    echo "<div class='alert alert-danger'>$passErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" name="cpass" value="<?php if(isset($_POST['cpass'])){echo $cpass;} ?>" class="form-control" placeholder="Confirm Password">
            <?php
                if(isset($cpassErr)){
                    echo "<div class='alert alert-danger'>$cpassErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>E-mail</label>
            <input type="text" name="email" value="<?php if(isset($_POST['email'])){echo $email;} ?>" class="form-control" placeholder="E-mail">
            <?php
                if(isset($emailErr)){
                    echo "<div class='alert alert-danger'>$emailErr</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Address</label>
            <input type="text" name="address1" value="<?php if(isset($_POST['address1'])){echo $address1;} ?>" class="form-control" placeholder="Address">
            <?php
                if(isset($address1Err)){
                    echo "<div class='alert alert-danger'>$address1Err</div>";
                }
            ?>
        </div>

        <div class="form-group">
            <label>Address</label>
            <input type="text" name="address2" value="<?php if(isset($_POST['address2'])){echo $address2;} ?>" class="form-control" placeholder="Address">
            <?php
                if(isset($address2Err)){
                    echo "<div class='alert alert-danger'>$address2Err</div>";
                }
            ?>
        </div>

        <button type="submit" name="add" class="btn btn-info">Submit</button>

    </form>
    
    </div>
    <div class="col-md-4"> </div>
</div>

<?php include('includes/footer.php'); ?>
