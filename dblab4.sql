-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2018 at 06:32 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dblab4`
--

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(10) NOT NULL,
  `blood_type` varchar(3) NOT NULL,
  `donor` varchar(50) NOT NULL,
  `hospital` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `blood_type`, `donor`, `hospital`, `created_at`, `updated_at`) VALUES
(1, 'O+', 'Kay Karen Andarino', 'St. Lukes Medical Center', '2018-02-22 15:56:16', '2018-02-23 00:17:29'),
(2, 'AB-', 'Kassell Andarino', 'Capitol Medical Center', '2018-02-22 15:57:00', '0000-00-00 00:00:00'),
(3, 'O-', 'Rebecca Goyena', 'Marikina Hospital', '2018-02-22 18:07:14', '0000-00-00 00:00:00'),
(4, 'A-', 'Kathy Lim', 'Valenzuela Hospital', '2018-02-22 18:08:46', '0000-00-00 00:00:00'),
(5, 'O+', 'GM Goyena', 'Marikina Hospital', '2018-02-22 18:12:44', '0000-00-00 00:00:00'),
(6, 'O-', 'Carolyn LIm', 'Valenzuela Hospital', '2018-02-22 18:12:44', '0000-00-00 00:00:00'),
(7, 'A+', 'Rosario Siervo', 'Manila Doctors Hospital', '2018-02-22 18:12:44', '0000-00-00 00:00:00'),
(8, 'A-', 'Isaac Siervo', 'Mateo Hospital', '2018-02-22 18:12:44', '0000-00-00 00:00:00'),
(9, 'B+', 'Mac Tolosa', 'Chinese General Hospital', '2018-02-22 18:12:44', '0000-00-00 00:00:00'),
(12, 'AB-', 'Barack Obama', 'The Medical City', '2018-02-23 01:41:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `id` int(10) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`id`, `fname`, `lname`, `username`, `pass`, `email`, `address1`, `address2`, `created_at`, `updated_at`) VALUES
(1, 'Jason', 'Siervo', 'jasiervo', 'ldislPUPfeu15', 'jason.siervo@gmail.com', 'blk 23 Lot 10', 'Sampaloc, Manila', '2018-02-21 05:53:09', '2018-02-21 16:20:20');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `id` int(10) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`id`, `fname`, `lname`, `username`, `pass`, `email`, `address1`, `address2`, `created_at`, `updated_at`) VALUES
(1, 'Angelo Isaac', 'Siervo', 'aasiervo', 'user1', 'angelo.siervo@gmail.com', 'Sta. Maria, Bulacan', '', '2018-02-21 05:56:57', '0000-00-00 00:00:00'),
(3, 'hilary shane', 'siervo', 'hasiervo', 'user123456', 'hilary.siervo@gmail.com', 'blk 34 lot 56', 'Fairview, Quezon City', '2018-02-22 12:26:38', '0000-00-00 00:00:00'),
(4, 'qweqwe', 'qweqwe', 'qweqwe', 'qweqweqwe', 'van.calimlim@gmail.com', 'Block 23 ', 'Quiapo, Manila', '2018-02-22 12:37:49', '2018-02-23 01:07:17'),
(7, 'Ben', 'Platt', 'bensplatt', 'evanhansen', 'ben.platt@gmail.com', 'blk 23 lot 10', 'New York, New York', '2018-02-23 04:12:54', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
