<!-- The Modal -->
<div id="myModal" class="modal">

	<!-- Modal content -->
	<div class="modal-content">
	  	<div class="modal-header">
			<span class="close">&times;</span>
			<br>
			<center><img src = "images/logo_new.png" style= "height:50px;"></center>
			<br>
		</div>

		<div class="modal-body">
			<center>

			<br>
			<br>
			<a href = "login_admin.php">
				<div id = "left">
					<span class="fas fa-id-badge fa-10x"></span>
					<br>
					<br>
					Administrator
					<br>
					<br>
				</div>
			</a>

			<a href = "login_user.php">
				<div id = "right">
					<span class="fas fa-user fa-10x"></span>
					<br>
					<br>
					Member
					<br>
					<br>
				</div>
			</a>

			<br style="clear:both;"/>

			</center>
		</div>
	</div>
</div>
