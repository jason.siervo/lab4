<?php include('includes/header.php'); ?>
<?php include('includes/navbar_login.php'); ?>

<?php
    session_start();
    
    if(isset($_POST['login'])) {
        include('includes/dbcon.php');

        $username1 = $_POST['username1'];
        $pass = $_POST['pass'];

        $res = $mysqli->query("SELECT * FROM tblusers WHERE username = '$username1' AND pass = '$pass'");

        if($res->num_rows == 0) {
            $_SESSION['error'] = "Invalid username or password";
            header("Location: login_user.php");
        } else {
            $_SESSION['username1'] = $username1;
            $_SESSION['pass'] = $pass;
            header("Location: dashboard_user.php");
        }
    }
?>

<div class = "part1_copy">
    <div class="col-md-4"> </div>
    <div class="col-md-4"> 
    
    <form method = "POST" action = "<?= $_SERVER['PHP_SELF']?>">
        <br>
        <h2>Member</h2>

        <?php 
         if(isset($_SESSION['error'])) {
             echo "<div class = 'alert alert-danger dismissable'>" .$_SESSION['error']. "</div>";
         }
        ?>

        <div class="form-group">
            <label>Username</label>
            <input type="text" name = "username1" class="form-control" placeholder="Username">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name = "pass" class="form-control" placeholder="Password">
        </div>
        <center>
        <button type="submit" name = "login" class="btn btn-info">Login</button>
        </center>
    </form>
    
    </div>
    <div class="col-md-4"> </div>
</div>

<?php include('includes/footer.php'); ?>